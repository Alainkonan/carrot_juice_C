#include <stdio.h>
#include "growing.h"
#include "pressing.h"

//new version

 
int main()
{
  int field[1000], carrots[1000];
  float juice;
  int i;
  
  plant(field, 500, 1);
  grow(field, 999, 3);
  harvest(field, carrots, 1000);
  printf("carrots[3]=%d\n", carrots[3]);
  juice = press(carrots, 999, 3.14);

  printf("%f carrot juices successfully created\n", juice);
  return 0;
}
